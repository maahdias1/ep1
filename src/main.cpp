#include <iostream>
#include <fstream>
#include <stdio.h>
#include "Candidato.hpp"
#include "pessoa.hpp"
#include "eleitor.hpp"
#include <map>
#include <string>
using namespace std;

void procuraCandidato(unsigned int numero_candidato, Candidato *candidatos, unsigned int total_candidatos);
void imprimeVoto(Eleitor eleitor);
int main()
{

    Candidato candidatosBR[26], candidatosDF[1237];
    for (unsigned int i = 0; i < 26; i++)
    {
        candidatosBR[i].setCandidato("./data/consulta_cand_2018_BR.csv", i);
    }
    for (unsigned int i = 0; i < 1237; i++)
    {
        candidatosDF[i].setCandidato("./data/consulta_cand_2018_DF.csv", i);
    }

    unsigned int numero_eleitores;
    cout << "Digite o numero de eleitores: ";
    cin >> numero_eleitores;
    map<string, int> candidatos_votos;
    candidatos_votos["DEP_FEDERAL_NULO"] = 0;
    candidatos_votos["DEP_ESTADUAL_NULO"] = 0;
    candidatos_votos["SENADOR_NULO"] = 0;
    candidatos_votos["GOVERNADOR_NULO"] = 0;
    candidatos_votos["PRESIDENTE_NULO"] = 0;
    candidatos_votos["DEP_FEDERAL_BRANCO"] = 0;
    candidatos_votos["DEP_ESTADUAL_BRANCO"] = 0;
    candidatos_votos["SENADOR_BRANCO"] = 0;
    candidatos_votos["GOVERNADOR_BRANCO"] = 0;
    candidatos_votos["PRESIDENTE_BRANCO"] = 0;
    for (unsigned int i = 0; i < 26; i++)
    {
        if (candidatosBR[i].getDS_CARGO().find("VICE-PRESIDENTE") == string::npos)
            candidatos_votos[to_string(candidatosBR[i].getNR_CANDIDATO()) + "-" + candidatosBR[i].getDS_CARGO()];
    }
    for (unsigned int i = 0; i < 1237; i++)
    {
        if (!candidatosDF[i].getDS_CARGO().compare("GOVERNADOR"))
            candidatos_votos[to_string(candidatosDF[i].getNR_CANDIDATO()) + "-" + candidatosDF[i].getDS_CARGO()];
        else if (candidatosDF[i].getDS_CARGO().compare("VICE-GOVERNADOR"))
            candidatos_votos[to_string(candidatosDF[i].getNR_CANDIDATO())];
    }

    Pessoa pessoas[numero_eleitores];
    Eleitor eleitores[numero_eleitores];
    string nome_pessoa;
    unsigned int dep_federal, dep_distrital, senador, governador, presidente;
    unsigned int confirma = 0;
    cout << "Para votar em BRANCO digite 0. \n";
    for (unsigned int i = 0; i < numero_eleitores; i++)
    {
        cin.ignore();
        cout << "Digite o nome do eleitor: ";
        getline(cin, nome_pessoa);
        do
        {
            pessoas[i].setNome(nome_pessoa);
            eleitores[i].setNome(nome_pessoa);
            cout << "Deputado Federal: ";
            cin >> dep_federal;
            cin.clear();
            procuraCandidato(dep_federal, candidatosDF, 1237);
            eleitores[i].setVotoDepFederal(dep_federal);
            cout << "Confirma 1, Corrige 2: ";
            cin >> confirma;
        } while (confirma != 1);
        candidatos_votos[to_string(dep_federal)]++;
        do
        {
            cout << "Deputado Distrital: ";
            cin >> dep_distrital;
            procuraCandidato(dep_distrital, candidatosDF, 1237);
            eleitores[i].setVotoDepDistrital(dep_distrital);
            cout << "Confirma 1, Corrige 2: ";
            cin >> confirma;
        } while (confirma != 1);
        candidatos_votos[to_string(dep_distrital)]++;
        do
        {
            cout << "Senador: ";
            cin >> senador;
            procuraCandidato(senador, candidatosDF, 1237);
            eleitores[i].setVotoSenador(senador);
            cout << "Confirma 1, Corrige 2: ";
            cin >> confirma;
        } while (confirma != 1);
        candidatos_votos[to_string(senador)]++;
        do
        {
            cout << "Governador: ";
            cin >> governador;
            procuraCandidato(governador, candidatosDF, 1237);
            eleitores[i].setVotoGovernador(governador);
            cout << "Confirma 1, Corrige 2: ";
            cin >> confirma;
        } while (confirma != 1);
        candidatos_votos[to_string(governador) + "-GOVERNADOR"]++;
        do
        {
            cout << "Presidente: ";
            cin >> presidente;
            procuraCandidato(presidente, candidatosBR, 26);
            eleitores[i].setVotoPresidente(presidente);
            cout << "Confirma 1, Corrige 2: ";
            cin >> confirma;
        } while (confirma != 1);
        cout << "VOTAÇÃO COMPUTADA \n";
        candidatos_votos[to_string(presidente) + "-PRESIDENTE"]++;

        // cout << "NOME: " << eleitores[i].getNome() << "VOTO DEP_FEDERAL: " << eleitores[i].getVotoDepFederal() << endl;
    }
    for (unsigned int i = 0; i < numero_eleitores; i++)
    {
        imprimeVoto(eleitores[i]);
    }
}

void procuraCandidato(unsigned int numero_candidato, Candidato *candidatos, unsigned int total_candidatos)
{
    unsigned int NR_CANDIDATO;
    unsigned int flag = 1;
    Candidato candidatoM, candidatoV;
    if (numero_candidato == 0)
    {
        cout << "DESEJA VOTAR BRANCO? \n";
    }
    for (unsigned int i = 0; i < total_candidatos; i++)
    {
        NR_CANDIDATO = candidatos[i].getNR_CANDIDATO();

        if (NR_CANDIDATO == numero_candidato)
        {
            if (NR_CANDIDATO < 99 && flag != 0)
            {
                flag = 0;
                if (candidatos[i].getDS_CARGO().find("VICE") != string::npos)
                {
                    candidatoV = candidatos[i];
                }
                else
                {
                    candidatoM = candidatos[i];
                }
            }
            else if (flag == 1)
            {
                candidatos[i].printCandidato();
                break;
            }
            else if (flag == 0)
            {
                if (candidatos[i].getDS_CARGO().find("VICE") != string::npos)
                {
                    candidatoV = candidatos[i];
                }
                else
                {
                    candidatoM = candidatos[i];
                }
                candidatos[i].printGorvernadorOuPresidente(candidatoM, candidatoV);
                break;
            }
        }
    }
}
void imprimeVoto(Eleitor eleitor)
{
    cout << "---------- VOTOS ----------" << endl;
    cout << "NOME: " << eleitor.getNome() << endl;
    cout << "DEPUTADO FEDERAL: ";
    if (eleitor.getVotoDepFederal() == 0)
    {
        cout << "BRANCO" << endl;
    }
    else
    {
        cout << eleitor.getVotoDepFederal() << endl;
    }
    cout << "DEPUTADO DISTRITAL: ";
    if (eleitor.getVotoDepDistrital() == 0)
    {
        cout << "BRANCO" << endl;
    }
    else
    {
        cout << eleitor.getVotoDepDistrital() << endl;
    }
    cout << "SENADOR: ";
    if (eleitor.getVotoSenador() == 0)
    {
        cout << "BRANCO" << endl;
    }
    else
    {
        cout << eleitor.getVotoSenador() << endl;
    }
    cout << "GOVERNADOR ";
    if (eleitor.getVotoGovernador() == 0)
    {
        cout << "BRANCO" << endl;
    }
    else
    {
        cout << eleitor.getVotoGovernador() << endl;
    }
    cout << "PRESIDENTE ";
    if (eleitor.getVotoPresidente() == 0)
    {
        cout << "BRANCO" << endl;
    }
    else
    {
        cout << eleitor.getVotoPresidente() << endl;
    }
    cout << "---------------------------" << endl;
}