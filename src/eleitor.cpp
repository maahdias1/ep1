#include <iostream>
#include "../inc/eleitor.hpp"
#include "../inc/pessoa.hpp"

using namespace std;

Eleitor::Eleitor()
{
    setNome("");
    setVotoPresidente(0);
    setVotoGovernador(0);
    setVotoSenador(0);
    setVotoDepFederal(0);
    setVotoDepDistrital(0);
};
Eleitor::~Eleitor(){

};
void Eleitor::setVotoPresidente(unsigned int votoPresidente)
{
    this->votoPresidente = votoPresidente;
}
unsigned int Eleitor::getVotoPresidente()
{
    return votoPresidente;
}
void Eleitor::setVotoGovernador(unsigned int votoGovernador)
{
    this->votoGovernador = votoGovernador;
}
unsigned int Eleitor::getVotoGovernador()
{
    return votoGovernador;
}
void Eleitor::setVotoSenador(unsigned int votoSenador)
{
    this->votoSenador = votoSenador;
}
unsigned Eleitor::getVotoSenador()
{
    return votoSenador;
}
void Eleitor::setVotoDepFederal(unsigned int votoDepFederal)
{
    this->votoDepFederal = votoDepFederal;
}
unsigned int Eleitor::getVotoDepFederal()
{
    return votoDepFederal;
}
void Eleitor::setVotoDepDistrital(unsigned int votoDepDistrital)
{
    this->votoDepDistrital = votoDepDistrital;
}
unsigned int Eleitor::getVotoDepDistrital()
{
    return votoDepDistrital;
}