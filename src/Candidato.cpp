#include <iostream>
#include <sstream>
#include "../inc/Candidato.hpp"
#include "../inc/pessoa.hpp"
#include <fstream>
#include <string>   
using namespace std;

Candidato::Candidato()
{
}
Candidato::~Candidato()
{
}
string remover_aspas(string linha_auxiliar);
void Candidato::setNM_UE(string NM_UE)
{
    this->NM_UE = NM_UE;
}

void Candidato::setDS_CARGO(string DS_CARGO)
{
    this->DS_CARGO = DS_CARGO;
}

void Candidato::setNR_CANDIDATO(unsigned int NR_CANDIDATO)
{
    this->NR_CANDIDATO = NR_CANDIDATO;
}

void Candidato::setNM_URNA_CANDIDATO(string NM_URNA_CANDIDATO)
{
    this->NM_URNA_CANDIDATO = NM_URNA_CANDIDATO;
}

void Candidato::setNM_PARTIDO(string NM_PARTIDO)
{
    this->NM_PARTIDO = NM_PARTIDO;
}

string Candidato::getNM_UE()
{
    return NM_UE;
}

string Candidato::getDS_CARGO()
{
    return DS_CARGO;
}

unsigned int Candidato::getNR_CANDIDATO()
{
    return NR_CANDIDATO;
}

string Candidato::getNM_URNA_CANDIDATO()
{
    return NM_URNA_CANDIDATO;
}

string Candidato::getNM_PARTIDO()
{
    return NM_PARTIDO;
}

void Candidato::setCandidato(const char *read_path, unsigned int numero_de_candidatos)
{
    string linha_leitura;
    ifstream arquivo;
    arquivo.open(read_path);
    if(arquivo.fail()) {
        cout << "ERROR \n";
    }
    unsigned int NM_UE_POS = (numero_de_candidatos + 1) * 57 + 12;
    unsigned int DS_CARGO_POS = (numero_de_candidatos + 1) * 57 + 14;
    unsigned int NR_CANDIDATO_POS = (numero_de_candidatos + 1) * 57 + 16;
    unsigned int NM_URNA_CANDIDATO_POS = (numero_de_candidatos + 1) * 57 + 18;
    unsigned int NM_PARTIDO_POS = (numero_de_candidatos + 1) * 57 + 29;
    for (unsigned int i = 0; i < (numero_de_candidatos + 2) * 58; i++)
    {   
        getline(arquivo, linha_leitura, ';');
        linha_leitura = remover_aspas(linha_leitura);
        if (i == NM_UE_POS)
        {
            // cout << linha_leitura << endl;
            this->setNM_UE(linha_leitura);
        }
        else if (i == DS_CARGO_POS)
        {
            // cout << linha_leitura << endl;
            this->setDS_CARGO(linha_leitura);
        }
        else if (i == NR_CANDIDATO_POS)
        {
            stringstream geek(linha_leitura);
            unsigned int linha_leitura_int = 0;
            geek >> linha_leitura_int;
            this->setNR_CANDIDATO(linha_leitura_int);
        }
        else if (i == NM_URNA_CANDIDATO_POS)
        {
            // cout << linha_leitura << endl;
            this->setNM_URNA_CANDIDATO(linha_leitura);
        }
        else if (i == NM_PARTIDO_POS)
        {
            // cout << linha_leitura << endl;
            this->setNM_PARTIDO(linha_leitura);
        }
    }
    arquivo.close();
}

void Candidato::printCandidato()
{
    cout << "Nome: " << NM_URNA_CANDIDATO << endl;
    cout << "Partido: " << NM_PARTIDO << endl;
}

void Candidato::printGorvernadorOuPresidente(Candidato candidatoM, Candidato candidatoV){
    cout << "Nome: " << candidatoM.getNM_URNA_CANDIDATO() << endl;
    cout << "Vice: " << candidatoV.getNM_URNA_CANDIDATO() << endl;
    cout << "Partido: " << NM_PARTIDO << endl;
}
string remover_aspas(string linha_parametro){
	int tam =0;
	string linha="";
	tam = linha_parametro.size();
	for (int i=1; i<tam-1; i++){
        linha += linha_parametro[i];
	}
	return linha;
}
