#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include <string>
#include "pessoa.hpp"

using namespace std;

class Eleitor : public Pessoa {
    private:
      unsigned int votoPresidente;
      unsigned int votoGovernador;
      unsigned int votoSenador;
      unsigned int votoDepFederal;
      unsigned int votoDepDistrital;
    
    public: 
      Eleitor();
      ~Eleitor(); 
      void setVotoPresidente (unsigned int votoPresidente);
      unsigned int getVotoPresidente();
      void setVotoGovernador (unsigned int votoGovernador);
      unsigned int getVotoGovernador();
      void setVotoSenador (unsigned int votoSenador);
      unsigned int getVotoSenador();
      void setVotoDepFederal (unsigned int votoDepFederal);
      unsigned int getVotoDepFederal();
      void setVotoDepDistrital (unsigned int votoDepDistrital);
      unsigned int getVotoDepDistrital();
      void imprimeVoto();
};

#endif