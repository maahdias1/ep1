#ifndef CANDIDADO_HPP
#define CANDIDATO_HPP

#include <string>
#include "pessoa.hpp"

using namespace std;

class Candidato : public Pessoa
{
  private:
    string NM_UE;
    string DS_CARGO;
    unsigned int NR_CANDIDATO;
    string NM_URNA_CANDIDATO;
    string NM_PARTIDO;
    void setNM_UE(string NM_UE);
    void setDS_CARGO(string DS_CARGO);
    void setNR_CANDIDATO(unsigned int NR_CANDIDATO);
    void setNM_URNA_CANDIDATO(string NM_URNA_CANDIDATO);
    void setNM_PARTIDO(string NM_PARTIDO);

  public:
    Candidato();
    ~Candidato();
    string getNM_UE();

    string getDS_CARGO();
    unsigned int getNR_CANDIDATO();
    string getNM_URNA_CANDIDATO();
    string getNM_PARTIDO();
    void printCandidato();
    void setCandidato(const char *read_path, unsigned int numero_de_candidatos);
    void printGorvernadorOuPresidente(Candidato candidatoM, Candidato candidatoV);
};

#endif