#ifndef _PESSOA_
#define _PESSOA_

#include <string>

using namespace std;

class Pessoa {
  protected:
     string nome;
  public:
     Pessoa();
     ~Pessoa();
     void setNome (string nome);
     string getNome();
};

#endif
